import styled from "@emotion/styled";
import DatePicker from "../../components/DatePicker/DatePicker";
import {useState} from "react";
import {set} from "date-fns";
import NotificationCard from "../../components/Cards/NotificationCard";
import LeftLessonsInfoBadge from "../../components/Badge/LeftLessonsInfoBadge";
import AddLessonButton from "../../components/Buttons/AddLessonButton";
import ActionCard from "../../components/Cards/ActionCard";
import LessonDetails from "../../components/LessonDetails/LessonDetails";

const HomePage = () => {
  const [startDate, setStartDate] = useState(new Date());
  const [showPreview, setShowPreview] = useState(false);
  const [showDetails, setShowDetails] = useState(false);

  const dateHandleChange = date => {
    setStartDate( date);
    setShowPreview(true)
  };

  const closePreviewHandler = () => {
    setShowPreview(false);
    setShowDetails(false)
  }

  const openDetailsHandler = () => {
    setShowDetails(true);
  }

  return (
    <div className="home-page">
      <WelcomeText>
        <span>Добро пожаловать,</span>
        <h4>Владимир!</h4>
      </WelcomeText>

      <NotificationCard/>

      <DatePickerContainer>
        <DatePicker value={startDate} onChange={dateHandleChange} lessonDays={lessonDays}/>
      </DatePickerContainer>

      <LeftLessonsInfoBadgeContainer>
        <LeftLessonsInfoBadge numberOfLessons={3}/>
        <AddLessonButton text={'Добавить уроки'}/>
      </LeftLessonsInfoBadgeContainer>

      <ActionCards>
        <ActionCard description="Краткое описание раздела с наградами ученика"
                    title="Мои награды"
                    type="primary"/>
        <ActionCard description="Потренируй свой навык ментальной арифметики."
                    title="Абакус"
                    type="default"/>
        <ActionCard description=""
                    title="Приведи друга и получи урок в подарок "
                    type="info"/>
      </ActionCards>

      <LessonDetails showPreview={showPreview}
                     showDetails={showDetails}
                     onClose={closePreviewHandler}
                     onOpenDetails={openDetailsHandler}/>
    </div>
  )
}

export default HomePage

const lessonDays = [
  {id: 1, date: set(new Date(), { date: 25 })},
  {id: 2, date: set(new Date(), { date: 17 })},
  {id: 3, date: set(new Date(), { date: 2 })}
]

const WelcomeText = styled.div`
  font-size: 22px;
  line-height: 30px;
  color: rgba(32, 32, 32, 0.75);
  display: flex;
  flex-direction: column;
  padding: 15px 0;
  
  h4 {
    align-self: start;
    border-bottom: 3px solid #B81199;  
  }
`;

const DatePickerContainer = styled.div`
  padding: 15px 0;
`;

const LeftLessonsInfoBadgeContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 15px;
`;

const ActionCards = styled.div``;