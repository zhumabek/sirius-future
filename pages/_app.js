import "react-datepicker/dist/react-datepicker.css";
import '../styles/style.scss';

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}