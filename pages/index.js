import HomePage from "./Home";
import NavBar from "../components/Navigation/NavBar";
import styled from "@emotion/styled";

const App = () => {
  return (
    <div>
      <NavBar/>
      <Container>
        <HomePage/>
      </Container>
    </div>
  )
};

const Container = styled.div`
  margin-top: 64px;
  padding: 15px;
`;

export default App;
