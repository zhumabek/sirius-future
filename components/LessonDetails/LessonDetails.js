import NoteBook from "../Navigation/SvgIcons/notebook.svg";
import styled from "@emotion/styled";
import ArrowUpIcon from "../Navigation/SvgIcons/arrow-up.svg";
import ClosePreviewButton from "../Buttons/CloseButton";

const LessonDetails = ({ showPreview, showDetails, onClose, onOpenDetails }) => {
  return (
    <Container showPreview={showPreview}
               showDetails={showDetails}>
      <Preview>
        {!showDetails ? (
          <MoreDetailsContainer>
            <ArrowUpIcon onClick={onOpenDetails}/>
          </MoreDetailsContainer>
        ): null }

        <PreviewContent>
          <IconContainer>
            <NoteBook/>
          </IconContainer>
          <PreviewDescriptionContainer>
            <h5>Курс ментальная арифметика</h5>
            <p>17 июня, 13:00-13:45</p>

            <PaymentStatusBadge>Оплачено</PaymentStatusBadge>
          </PreviewDescriptionContainer>
          <ClosePreviewButton onClick={onClose}/>
        </PreviewContent>
      </Preview>

      <Details>
        Here goes Lesson Details
      </Details>
    </Container>
  )
}

export default LessonDetails;

const Container = styled.div`
  width: 100%;
  box-sizing: border-box;
  position: fixed;
  bottom: 0;
  left: 0;

  
  z-index: 200;
  transition: transform 1s; 
  
  transform: ${props => {
    if(props.showDetails){
      return "translateY(0)"
    } else if(props.showPreview){
      return "translateY(77%)"
    } else {
      return "translateY(100%)"
    }
  }};
`;

const Preview = styled.div`
  background: linear-gradient(110.45deg, #F64867 7.06%, #BA2D86 95.26%);
  box-shadow: 0px 1px 4px rgba(90, 49, 100, 0.226972);
  padding: 20px 15px;
`;

const PreviewContent = styled.div`
  display: flex;

`;

const IconContainer = styled.div`
  margin-right: 15px;
`;

const PreviewDescriptionContainer = styled.div`
  font-weight: 600;
  font-size: 16px;
  line-height: 22px;
  margin-right: auto;
  color: #FFFFFF;

  h5 {
    font-weight: 600;
    font-size: 16px;
    line-height: 22px;
    margin: 0;
    padding: 0;
  }
   
  p {
    font-size: 14px;
    line-height: 19px;
    opacity: 0.7;
  }
`;

const PaymentStatusBadge = styled.div`
  font-weight: 800;
  font-size: 10px;
  line-height: 14px;
  text-align: center;
  text-transform: uppercase;
  width: 79px;
  padding: 7px 10px;
  background: #1AC9B7;
  border-radius: 50px;
  margin-top: 10px;
`;

const MoreDetailsContainer = styled.div`
  display: flex;
  justify-content: center;
  padding-bottom: 15px;
`;

const Details = styled.div`
  padding: 20px 15px;
  background: #FFFFFF;
  box-shadow: 0px 1px 4px rgba(90, 49, 100, 0.226972);
  height: 315px;
`;