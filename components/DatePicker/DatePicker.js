import React from "react";
import ReactDatePicker from "react-datepicker";
import { registerLocale } from  "react-datepicker";
import ru from 'date-fns/locale/ru';
import { format } from 'date-fns'
import styled from "@emotion/styled";
registerLocale('ru', ru);

const DatePicker = (props) => {
  let { value, onChange, lessonDays } = props;

  const isLessonDay = (lessonDays, dateToCompare) => {
    let classDayHighlight = null;
    let formattedDateToCompare = format(dateToCompare, "dd-MM-yyyy");

    for(let lessonDay of lessonDays) {
      let formattedLessonDate = format(lessonDay.date, "dd-MM-yyyy");

      if (formattedLessonDate === formattedDateToCompare){
        classDayHighlight = <ClassDayHighlight/>
        break;
      }
    }

    return classDayHighlight;
  }

  const renderDayContents = (day, date) => {
    return (
      <ClassDay>
        <span>{day}</span>
        {isLessonDay(lessonDays, date)}
      </ClassDay>
    );
  };

  return (
    <ReactDatePicker
      selected={value}
      onChange={onChange}
      renderDayContents={renderDayContents}
      locale="ru"
      inline
    />
  );
};

export default DatePicker;

const ClassDayHighlight = styled.span`
  width: 5px;
  height: 5px;
  border-radius: 50%;
  background-color: #B81199;
`;

const ClassDay = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 32px;
    height: 32px;
`;
