import React from "react";
import styled from "@emotion/styled";
import NextButton from "../Buttons/NextButton";
import {appColors} from "../../utils/colors";

const ActionCard = ({ title, description, type }) => {
  let cardColor = appColors.default;
  if (type){
    cardColor = appColors[type];
  }

  return (
    <Card gradient={cardColor.gradient}>
      <CardText>
        <h5>{title}</h5>
        {description ? <p>{description}</p> : null}
      </CardText>
      <CardAction>
        <NextButton color={type}/>
      </CardAction>
    </Card>
  )
};

export default ActionCard;

const Card = styled.div`
  width: 100%;
  height: 110px;
  box-sizing: border-box;
  background: ${props => props.gradient };
  box-shadow: 0px 1px 4px rgba(90, 49, 100, 0.226972);
  border-radius: 10px;
  padding: 15px 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 15px;
`;

const CardText = styled.div`
  color: #FFFFFF;
  
  h5 {
    font-weight: 600;
    font-size: 17px;
    line-height: 23px;
  }
  
  p {
    font-size: 13px;
    line-height: 18px;
    opacity: 0.8;
    padding: 10px 0;
  }
  
`;

const CardAction = styled.div``;