import React from "react";
import styled from "@emotion/styled";
import NextButton from "../Buttons/NextButton";

const NotificationCard = () => {
 return (
   <Card>
    <CardText>
      <p>Следующий урок через:</p>
      <p>04 <span>ч.</span> 12 <span>мин</span></p>
    </CardText>
     <NextButton/>
   </Card>
 )
};

export default NotificationCard;

const Card = styled.div`
  width: 100%;
  height: 80px;
  box-sizing: border-box;
  background: linear-gradient(192.41deg, #C71EB4 -5.64%, #6F2FB8 94.22%);
  box-shadow: 0px 1px 4px rgba(90, 49, 100, 0.226972);
  border-radius: 10px;
  padding: 0 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 15px;
`;

const CardText = styled.div`
  color: #FFFFFF;
  
  p:first-of-type {
    font-weight: bold;
    font-size: 13px;
    line-height: 18px;
  }
  
  p:last-of-type {
    font-weight: 600;
    font-size: 24px;
    line-height: 33px;
    opacity: 0.6;
    
    span {
      font-size: 13px;
      line-height: 18px;
    }
  }
  
`;