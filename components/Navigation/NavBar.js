import styled from '@emotion/styled'
import {css} from '@emotion/core'
import {useState} from "react";
import NavItems from "./NavItems";
import MainLogo from "./SvgIcons/main-logo.svg";
import LogoTextIcon from "./SvgIcons/logo-text.svg";
import Achievements from "./Achievements";

const NavBar = () => {
  const [showSideMenu, setShowSideMenu] = useState(false);

  return (
  <Header showSideMenu={showSideMenu}>
    <Nav showSideMenu={showSideMenu}>
      <TopNav>
        <HamburgerIcon showSideMenu={showSideMenu} onClick={() => setShowSideMenu(previousState => !previousState)}>
          &nbsp;
        </HamburgerIcon>

        {!showSideMenu ? <LogoTextIcon/> : null }

        <PhotoButtonWrapper>
          {showSideMenu ? <Achievements amountOfAchievements={3}/> : null }
          <PhotoButton/>
        </PhotoButtonWrapper>
      </TopNav>

      <SideMenu showSideMenu={showSideMenu}>
        <HorizontalLine/>
        <div>
          <NavItems/>
        </div>

        <Logo>
          <MainLogo/>
        </Logo>
      </SideMenu>
    </Nav>
  </Header>
  );
}

export default NavBar;

const showSideMenuStyles = css`
  background-color: transparent;
  
  &::before {
    top: 0;
    transform: rotate(135deg);
  }

  &::after {
    top: 0;
    transform: rotate(-135deg);
  }
`;


const HamburgerIcon = styled.span`
    position: relative;
    width: 28px;
    height: 2px;
    background-color: white;
    border-radius: 10%;
    display: inline-block;
    
    &::before,
    &::after {
        content: "";
        width: 100%;
        height: 100%;   
        background-color: white;
        border-radius: 10%;
        display: inline-block;
        position: absolute;
        left: 0;
        transition: all .2s;
    }
    
    &::before { top: -.5rem; }
    &::after { top: .5rem; }

    ${props => props.showSideMenu ? showSideMenuStyles : "" }  
`;

const PhotoButton = styled.div`
  position:relative;
  background-color: white;
  width: 30px;
  height: 30px;
  box-sizing: border-box;
  border: double 3px transparent;
  border-radius: 50%;
  background-image: linear-gradient(white, white), 
                    linear-gradient(135deg, red, #2f0633, #2f0633);
  background-origin: border-box;
  background-clip: content-box, border-box;
  cursor: pointer;
`;

const Nav = styled.nav`
  box-sizing: border-box;
  background: linear-gradient(0deg, #690297 0%, #BF1399 100%);
  padding: 0 15px;
  
  overflow-y: ${props => props.showSideMenu ? "scroll" : ""};
  max-height: ${props => props.showSideMenu ? "100%" : ""};
  transition: max-height 1s;
`;

const TopNav = styled.div`
  box-sizing: border-box;
  height: 64px;
  padding-top: 15px;
  padding-bottom: 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const HorizontalLine = styled.hr`
  height: 2px;
  background: #FFFFFF;
  opacity: 0.2;
  border-radius: 21px;
  margin: 0 0 15px 0;
`;


const Logo = styled.div`
  padding: 30px;
  display: flex;
  justify-content: center; 
`;

const SideMenu = styled.div`
  max-height: ${props => props.showSideMenu ? "640px" : "0"}; 
  transform: ${props => props.showSideMenu ? "translateX(0)" : "translateX(-120%)" }; 
  transition: transform 1s, max-height 1s;
`;

const PhotoButtonWrapper = styled.div`
  display: flex;
`;

const Header = styled.header`
  position: fixed;
  top: 0;
  width: 100vw;
  height: ${props => props.showSideMenu ? "100vh" : "" };
  z-index: 100;
`;
