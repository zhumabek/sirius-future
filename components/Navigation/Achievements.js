import StarIconWrapper from "./SvgIcons/star-icon-wrapper.svg"
import StarIcon from "./SvgIcons/star.svg"
import StarIconBackground from "./SvgIcons/star-icon-background.svg"
import styled from "@emotion/styled";

const Achievements = ({amountOfAchievements}) => {

  return (
    <BlockContainer>
      {
        [...Array(amountOfAchievements)].map((i) => {
          return (
            <AchievementItem key={i}>
              <StarIconBackground/>
              <StarIconWrapper/>
              <StarIcon/>
            </AchievementItem>
          )
        })
      }
    </BlockContainer>
  )
};

export default Achievements;

const AchievementItem = styled.div`
  position: relative;
  display: inline-block;
  
  & > svg:first-of-type {
    position: absolute;
    left: 50%;
    top: 30%;
    transform: translate(-50%, -50%);
  }
  
  & > svg:last-of-type {
    position: absolute;
    left: 50%;
    top: 36%;
    transform: translate(-50%, -50%);
  }
  
  & > svg:nth-last-of-type(2) {
    position: relative;
  }
  
  &:not(:last-of-type) {
    margin-right: -17px;
  }
`;

const BlockContainer = styled.div`
  margin-right: 10px;
`;