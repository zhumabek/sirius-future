import Link from "next/link";
import ScheduleIcon from "./SvgIcons/schedule.svg"
import MentalArithmeticIcon from "./SvgIcons/mental-arithmetic.svg"
import NoteBooKIcon from "./SvgIcons/notebook.svg"
import BalanceIcon from "./SvgIcons/balance.svg"
import ContactIcon from "./SvgIcons/contact-with-manager.svg"
import SettingsIcon from "./SvgIcons/settings.svg"
import CameraSettingsIcon from "./SvgIcons/check-camera-settings.svg"
import LogoutIcon from "./SvgIcons/logout.svg"
import styled from "@emotion/styled";

const NavItems = () => {
  return (
    <List>
      <ListItem>
        <Link href="/">
          <MenuItem>
            <Icon>
              <ScheduleIcon/>
            </Icon>
            <span>Расписание занятий</span>
          </MenuItem>
        </Link>
      </ListItem>
      <ListItem>
        <Link href="/">
          <MenuItem>
            <Icon>
              <MentalArithmeticIcon/>
            </Icon>
            <span>Ментальная арифметика</span>
          </MenuItem>
        </Link>
      </ListItem>
      <ListItem>
        <Link href="/">
          <MenuItem>
            <Icon>
              <NoteBooKIcon/>
            </Icon>
            <span>Тетрадь</span>
          </MenuItem>
        </Link>
      </ListItem>
      <ListItem>
        <Link href="/">
          <MenuItem>
            <Icon>
              <BalanceIcon/>
            </Icon>
            <span>Баланс</span>
          </MenuItem>
        </Link>
      </ListItem>
      <ListItem>
        <Link href="/">
          <MenuItem>
            <Icon>
              <SettingsIcon/>
            </Icon>
            <span>Настройки</span>
          </MenuItem>
        </Link>
      </ListItem>
      <ListItem>
        <Link href="/">
          <MenuItem>
            <Icon>
              <ContactIcon/>
            </Icon>
            <span>Связаться с менеджером</span>
          </MenuItem>
        </Link>
      </ListItem>
      <ListItem>
        <Link href="/">
          <MenuItem>
            <Icon>
              <CameraSettingsIcon/>
            </Icon>
            <span>Проверить камеру и звук</span>
          </MenuItem>
        </Link>
      </ListItem>
      <ListItem>
        <Link href="/">
          <MenuItem>
            <Icon>
              <LogoutIcon/>
            </Icon>
            <span>Выйти</span>
          </MenuItem>
        </Link>
      </ListItem>
    </List>
  )
};

export default NavItems;

const MenuItem = styled.div`
  display: flex;
  align-items: center;
  font-weight: 600;
  font-size: 14px;
  line-height: 19px;
  color: #FFFFFF;  
`;

const Icon = styled.div`
    display: flex;
    flex-basis: 30px;
    padding: 15px;
    
`;

const ListItem = styled.li``;

const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`;
