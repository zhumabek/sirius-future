import styled from "@emotion/styled";

const LeftLessonsInfoBadge = ({numberOfLessons}) => {
  return (
    <BadgeContainer>
      <BadgeText>Осталось уроков:</BadgeText>
      <Badge>{numberOfLessons}</Badge>
    </BadgeContainer>
  )
};

export default LeftLessonsInfoBadge;

const BadgeContainer = styled.div`
  display: flex;
  align-items: center;
`;

const Badge = styled.div`
  width: 42px;
  height: 42px;
  background: linear-gradient(99.33deg, #F64867 7.06%, #BA2D86 95.26%);
  border-radius: 50%;
  color: #FFFFFF;
  font-weight: 600;
  font-size: 18px;
  line-height: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
`;


const BadgeText = styled.p`
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;
  color: #2A2A3B;
  width: 73px;
`;