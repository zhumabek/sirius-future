import styled from "@emotion/styled";

const AddLessonButton = ({text, onClick}) => {
  return <StyledButton onClick={onClick}>{text}</StyledButton>
}

export default AddLessonButton;

const StyledButton = styled.button`
  text-transform: uppercase;
  color: #FFFFFF;
  border: none;
  padding: 13px 20px;
  background: linear-gradient(120.06deg, #F64867 7.06%, #BA2D86 95.26%);
  border-radius: 32px;
  
  &:focus, &:visited {
    outline: none;
  }
`;