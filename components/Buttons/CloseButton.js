import React from "react";
import styled from "@emotion/styled";
const ClosePreviewButton = ({onClick}) => {

  return (
      <StyledButton onClick={onClick}>
        <span>&#10005;</span>
      </StyledButton>
  )
};

export default ClosePreviewButton;

const StyledButton = styled.div`
  width: 40px;
  height: 40px;
  background: #FFFFFF;
  border: 1px solid #FFFFFF;
  box-sizing: border-box;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 5px 10px 25px rgba(0, 0, 0, 0.75);
  
  span {
    color: #BF2F83;;
  }
`;