import React from "react";
import styled from "@emotion/styled";
import { appColors } from "../../utils/colors";
const NextButton = ({color, onClick}) => {
  let iconColor = appColors.default.color;
  if (color){
    iconColor = appColors[color].color;
  }

  return (
      <StyledButton color={iconColor} onClick={onClick}>
        <span>&#10148;</span>
      </StyledButton>
  )
};

export default NextButton;

const StyledButton = styled.div`
  width: 40px;
  height: 40px;
  background: #FFFFFF;
  border: 1px solid #FFFFFF;
  box-sizing: border-box;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 5px 10px 25px rgba(0, 0, 0, 0.75);
  
  span {
    color: ${props => props.color};
  }
`;