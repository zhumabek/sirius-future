export const appColors = {
  default: {
    color: "#540182",
    gradient: "linear-gradient(180deg, #B00CC8 0%, #600398 100%)",
  },
  primary: {
    color: "#4A42F6",
    gradient: "linear-gradient(199.15deg, #334FFE 7.38%, #6C2FEB 95.63%)"
  },
  info: {
    color: "#2A92A9",
    gradient: "linear-gradient(187.95deg, #1AC9B7 10.29%, #4DA8EE 90.89%)"
  },
}